package cn.zcltd.btg.tools;

import cn.zcltd.btg.sutil.EmptyUtil;
import com.alibaba.fastjson.JSONArray;

/**
 * 快速构建JsonArray对象
 * ps:取名为JsonQ的意义在于，Q位于键盘左上角
 * ，由左手小指负责，小指平时使用较少
 * ，这样可以多锻炼，利于开发右脑
 */
public class JsonQArray extends JSONArray {

    public JsonQArray() {

    }

    public static JsonQArray by(JsonQ jsonQ) {
        return new JsonQArray().ad(jsonQ);
    }

    public static JsonQArray byo(Object obj) {
        return new JsonQArray().ado(obj);
    }

    public static JsonQArray create() {
        return new JsonQArray();
    }

    public JsonQArray ad(JsonQ jsonQ) {
        if (EmptyUtil.isNotEmpty(jsonQ)) {
            super.add(jsonQ);
        }
        return this;
    }

    public JsonQArray ad(JSONArray jsonArray) {
        super.addAll(jsonArray);
        return this;
    }

    public JsonQArray ado(Object obj) {
        if (EmptyUtil.isNotEmpty(obj)) {
            super.add(obj);
        }
        return this;
    }

    public JsonQ getJsonQ(int index) {
        return JsonQ.create().set(this.getJSONObject(index));
    }
}