package cn.zcltd.btg.sutil;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * util:签名处理
 */
public class SignUtil {

    /**
     * 将参数map按照key排序
     *
     * @param params 参数map
     * @return 排序后map
     */
    public static Map<String, String> sort4string(Map<String, String> params) {
        Map<String, String> sortMap = new TreeMap<String, String>(new Comparator<String>() {

            @Override
            public int compare(String s1, String s2) {
                return s1.compareTo(s2);
            }
        });

        sortMap.putAll(params);

        return sortMap;
    }

    /**
     * 将参数map按照key排序
     *
     * @param params 参数map
     * @return 排序后map
     */
    public static Map<String, String> sort4object(Map<String, Object> params) {
        Map<String, String> sortMap = new TreeMap<String, String>(new Comparator<String>() {

            @Override
            public int compare(String s1, String s2) {
                return s1.compareTo(s2);
            }
        });

        for (Map.Entry<String, Object> entry : params.entrySet()) {
            sortMap.put(entry.getKey(), String.valueOf(entry.getValue()));
        }

        return sortMap;
    }

    /**
     * 获取参数map按照key排序并使用url格式组装结果
     *
     * @param params 参数map
     * @return 组装结果
     */
    public static String getKVStr4String(Map<String, String> params) {
        Map<String, String> sortMap = sort4string(params);

        StringBuilder signStrSb = new StringBuilder();
        for (Map.Entry entry : sortMap.entrySet()) {
            signStrSb.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }

        return signStrSb.length() > 0 ? signStrSb.substring(1) : signStrSb.toString();
    }

    /**
     * 获取参数map按照key排序并使用url格式组装结果
     *
     * @param params 参数map
     * @return 组装结果
     */
    public static String getKVStr4Object(Map<String, Object> params) {
        Map<String, String> sortMap = sort4object(params);

        StringBuilder signStrSb = new StringBuilder();
        for (Map.Entry entry : sortMap.entrySet()) {
            signStrSb.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }

        return signStrSb.length() > 0 ? signStrSb.substring(1) : signStrSb.toString();
    }
}
