package cn.zcltd.btg.sutil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 工具类：时间处理
 */
public class DateUtil {
    public static final String DEFAULT_PATTERN_YEAR = "yyyy";
    public static final String DEFAULT_PATTERN_MONTH = "MM";
    public static final String DEFAULT_PATTERN_DAY = "dd";
    public static final String DEFAULT_PATTERN_HOUR = "HH";
    public static final String DEFAULT_PATTERN_MINUTE = "mm";
    public static final String DEFAULT_PATTERN_SECOND = "ss";
    public static final String DEFAULT_PATTERN_MILLISECOND = "SSS";
    public static final String DEFAULT_PATTERN_DATE = "yyyy-MM-dd";
    public static final String DEFAULT_PATTERN_TIME = "HH:mm:ss";
    public static final String DEFAULT_PATTERN_DATETIME = "yyyy-MM-dd HH:mm:ss";
    public static final String DEFAULT_PATTERN_TIMESTAMP = "yyyyMMddHHmmssSSS";

    private DateUtil() {

    }

    /**
     * 获取当前系统时间
     *
     * @return 时间对象
     */
    public static Date getNow() {
        return new Date();
    }

    /**
     * 获取格式化日期或时间
     *
     * @param date          时间对象
     * @param formatPattern 格式化字符串
     * @return String
     */
    public static String format(Date date, String formatPattern) {
        if (EmptyUtil.isEmpty(date)) return "";
        if (EmptyUtil.isEmpty(formatPattern)) formatPattern = DEFAULT_PATTERN_DATETIME;
        return new SimpleDateFormat(formatPattern).format(date);
    }

    /**
     * 获取格式化年
     *
     * @param date 时间对象
     * @return String
     */
    public static String formatYear(Date date) {
        return format(date, DEFAULT_PATTERN_YEAR);
    }

    /**
     * 获取格式化月
     *
     * @param date 时间对象
     * @return String
     */
    public static String formatMonth(Date date) {
        return format(date, DEFAULT_PATTERN_MONTH);
    }

    /**
     * 获取格式化日
     *
     * @param date 时间对象
     * @return String
     */
    public static String formatDay(Date date) {
        return format(date, DEFAULT_PATTERN_DAY);
    }

    /**
     * 获取格式化时
     *
     * @param date 时间对象
     * @return String
     */
    public static String formatHour(Date date) {
        return format(date, DEFAULT_PATTERN_HOUR);
    }

    /**
     * 获取格式化分
     *
     * @param date 时间对象
     * @return String
     */
    public static String formatMinute(Date date) {
        return format(date, DEFAULT_PATTERN_MINUTE);
    }

    /**
     * 获取格式化秒
     *
     * @param date 时间对象
     * @return String
     */
    public static String formatSecond(Date date) {
        return format(date, DEFAULT_PATTERN_SECOND);
    }

    /**
     * 获取格式化毫秒
     *
     * @param date 时间对象
     * @return String
     */
    public static String formatMillisecond(Date date) {
        return format(date, DEFAULT_PATTERN_MILLISECOND);
    }

    /**
     * 获取格式化日期
     *
     * @param date 时间对象
     * @return String
     */
    public static String formatDate(Date date) {
        return format(date, DEFAULT_PATTERN_DATE);
    }

    /**
     * 获取格式化时间
     *
     * @param date 时间对象
     * @return String
     */
    public static String formatTime(Date date) {
        return format(date, DEFAULT_PATTERN_TIME);
    }

    /**
     * 获取格式化日期和时间
     *
     * @param date 时间对象
     * @return String
     */
    public static String formatDateTime(Date date) {
        return format(date, DEFAULT_PATTERN_DATETIME);
    }

    /**
     * 获取格式化时间戳
     *
     * @param date 时间对象
     * @return String
     */
    public static String formatTimestamp(Date date) {
        return format(date, DEFAULT_PATTERN_TIMESTAMP);
    }

    /**
     * 获取当前系统日期
     *
     * @return String
     */
    public static String getNowDate() {
        return formatDate(getNow());
    }

    /**
     * 获取当前系统时间
     *
     * @return String
     */
    public static String getNowTime() {
        return formatTime(getNow());
    }

    /**
     * 获取当前系统日期和时间
     *
     * @return String
     */
    public static String getNowDateTime() {
        return formatDateTime(getNow());
    }

    /**
     * 获取时间戳
     *
     * @return String
     */
    public static String getNowTimestamp() {
        return formatTimestamp(getNow());
    }

    /**
     * 获取指定日期所在周开始日期
     *
     * @param date 日期
     * @return 所在周开始日期
     */
    public static Date getWeekFirstDay(Date date) {
        Calendar c = DateUtil.date2Calendar(date);
        c.setFirstDayOfWeek(Calendar.MONDAY);
        int weekYear = c.get(Calendar.YEAR);
        int weekOfYear = c.get(Calendar.WEEK_OF_YEAR);
        c.setWeekDate(weekYear, weekOfYear, Calendar.MONDAY);
        return c.getTime();
    }

    /**
     * 获取指定日期所在周结束日期
     *
     * @param date 日期
     * @return 所在周结束日期
     */
    public static Date getWeekLastDay(Date date) {
        Calendar c = DateUtil.date2Calendar(date);
        c.setFirstDayOfWeek(Calendar.MONDAY);
        int weekYear = c.get(Calendar.YEAR);
        int weekOfYear = c.get(Calendar.WEEK_OF_YEAR);
        c.setWeekDate(weekYear, weekOfYear, Calendar.SUNDAY);
        return c.getTime();
    }

    /**
     * 使用参数Format将字符串转为Date
     *
     * @param strDate 日期字符串
     * @param pattern 格式化字符串
     * @return Date对象
     */
    public static Date parse(String strDate, String pattern) {
        if (EmptyUtil.isEmpty(strDate)) return null;
        if (EmptyUtil.isEmpty(pattern)) pattern = DEFAULT_PATTERN_DATETIME;
        try {
            return new SimpleDateFormat(pattern).parse(strDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 将日期字符串转化为Date对象(格式2001-01-01)
     *
     * @param strDate 日期字符串
     * @return Date对象
     */
    public static Date parseDate(String strDate) {
        return parse(strDate, DEFAULT_PATTERN_DATE);
    }

    /**
     * 将日期字符串转化为Date对象(格式12:30:30)
     *
     * @param strDate 日期字符串
     * @return Date对象
     */
    public static Date parseTime(String strDate) {
        return parse(strDate, DEFAULT_PATTERN_TIME);
    }

    /**
     * 将日期字符串转化为Date对象(格式2001-01-01 12:30:30)
     *
     * @param strDate 日期字符串
     * @return Date对象
     */
    public static Date parseDateTime(String strDate) {
        return parse(strDate, DEFAULT_PATTERN_DATETIME);
    }

    /**
     * 将日期字符串转化为Date对象(格式20010101123030222)
     *
     * @param strDate 日期字符串
     * @return Date对象
     */
    public static Date parseTimestamp(String strDate) {
        return parse(strDate, DEFAULT_PATTERN_TIMESTAMP);
    }

    /*
        =======================Calendar分割线==========================
     */

    /**
     * 将Date转换为Calendar
     *
     * @param date 日期
     * @return Calendar
     */
    public static Calendar date2Calendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * 日期计算
     *
     * @param date   日期
     * @param field  单位
     * @param amount 数量
     * @return Date
     */
    public static Date dateAdd(Date date, int field, int amount) {
        Calendar calendar = date2Calendar(date);
        calendar.add(field, amount);
        return calendar.getTime();
    }
}