package cn.zcltd.btg.sutil;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

/**
 * 工具类:验证是否为空
 */
public class EmptyUtil {

    private EmptyUtil() {
    }

    /**
     * 验证对象是否为空
     *
     * @param obj 待验证对象
     * @return boolean
     */
    public static boolean isEmpty(Object obj) {
        if (null == obj) return true;
        if (obj instanceof Collection) {
            return ((Collection) obj).isEmpty();
        }
        if (obj instanceof Map) {
            return ((Map) obj).isEmpty();
        }
        if (obj instanceof CharSequence) {
            return ((CharSequence) obj).toString().trim().length() == 0;
        }
        if (obj.getClass().isArray()) {
            return Array.getLength(obj) == 0;
        }
        return false;
    }

    /**
     * 验证对象是否不为空
     *
     * @param obj 待验证对象
     * @return boolean
     */
    public static boolean isNotEmpty(Object obj) {
        return !isEmpty(obj);
    }
}
