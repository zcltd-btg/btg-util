package cn.zcltd.btg.sutil;

import java.util.HashMap;
import java.util.Map;

/**
 * util:url解析
 */
public class UrlUtil {

    /**
     * 解析url
     *
     * @param url url地址
     * @return 解析结果
     */
    public static UrlEntity parse(String url) {
        UrlEntity entity = new UrlEntity();

        if (EmptyUtil.isEmpty(url)) {
            return entity;
        }

        url = url.trim();
        String[] urlParts = url.split("\\?");
        entity.setBaseUrl(urlParts[0]);

        if (urlParts.length == 1) {//没有参数
            return entity;
        }

        //有参数
        String[] params = urlParts[1].split("&");
        for (String param : params) {
            String[] keyValue = param.split("=");
            entity.getParams().put(keyValue[0], keyValue[1]);
        }

        return entity;
    }

    public static class UrlEntity {
        private String baseUrl;
        private Map<String, String> params = new HashMap<>();

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public Map<String, String> getParams() {
            return params;
        }

        public void setParams(Map<String, String> params) {
            this.params = params;
        }
    }
}