package cn.zcltd.btg.sutil;

import org.junit.Test;

import java.util.Calendar;

/**
 * DateUtil Tester.
 */
public class DateUtilTest {
    /**
     * Method: getNow()
     */
    @Test
    public void testGetNow() throws Exception {
        System.out.println(DateUtil.getNow().toString());
    }

    /**
     * Method: format(Date date, String formatPattern)
     */
    @Test
    public void testFormat() throws Exception {
        System.out.println(DateUtil.format(DateUtil.getNow(), DateUtil.DEFAULT_PATTERN_DATE));
    }

    /**
     * Method: formatYear(Date date)
     */
    @Test
    public void testFormatYear() throws Exception {
        System.out.println(DateUtil.formatYear(DateUtil.getNow()));
    }

    /**
     * Method: formatMonth(Date date)
     */
    @Test
    public void testFormatMonth() throws Exception {
        System.out.println(DateUtil.formatMonth(DateUtil.getNow()));
    }

    /**
     * Method: formatDay(Date date)
     */
    @Test
    public void testFormatDay() throws Exception {
        System.out.println(DateUtil.formatDay(DateUtil.getNow()));
    }

    /**
     * Method: formatHour(Date date)
     */
    @Test
    public void testFormatHour() throws Exception {
        System.out.println(DateUtil.formatHour(DateUtil.getNow()));
    }

    /**
     * Method: formatMinute(Date date)
     */
    @Test
    public void testFormatMinute() throws Exception {
        System.out.println(DateUtil.formatMinute(DateUtil.getNow()));
    }

    /**
     * Method: formatSecond(Date date)
     */
    @Test
    public void testFormatSecond() throws Exception {
        System.out.println(DateUtil.formatSecond(DateUtil.getNow()));
    }

    /**
     * Method: formatMillisecond(Date date)
     */
    @Test
    public void testFormatMillisecond() throws Exception {
        System.out.println(DateUtil.formatMillisecond(DateUtil.getNow()));
    }

    /**
     * Method: formatDate(Date date)
     */
    @Test
    public void testFormatDate() throws Exception {
        System.out.println(DateUtil.formatDate(DateUtil.getNow()));
    }

    /**
     * Method: formatTime(Date date)
     */
    @Test
    public void testFormatTime() throws Exception {
        System.out.println(DateUtil.formatTime(DateUtil.getNow()));
    }

    /**
     * Method: formatDateTime(Date date)
     */
    @Test
    public void testFormatDateTime() throws Exception {
        System.out.println(DateUtil.formatDateTime(DateUtil.getNow()));
    }

    /**
     * Method: formatTimestamp(Date date)
     */
    @Test
    public void testFormatTimestamp() throws Exception {
        System.out.println(DateUtil.formatTimestamp(DateUtil.getNow()));
    }

    /**
     * Method: getNowDate()
     */
    @Test
    public void testGetNowDate() throws Exception {
        System.out.println(DateUtil.getNowDate());
    }

    /**
     * Method: getNowTime()
     */
    @Test
    public void testGetNowTime() throws Exception {
        System.out.println(DateUtil.getNowTime());
    }

    /**
     * Method: getNowDateTime()
     */
    @Test
    public void testGetNowDateTime() throws Exception {
        System.out.println(DateUtil.getNowDateTime());
    }

    /**
     * Method: getNowTimestamp()
     */
    @Test
    public void testGetNowTimestamp() throws Exception {
        System.out.println(DateUtil.getNowTimestamp());
    }

    /**
     * Method: parse(String strDate, String pattern)
     */
    @Test
    public void testParse() throws Exception {
        System.out.println(DateUtil.parse(DateUtil.getNowTimestamp(), DateUtil.DEFAULT_PATTERN_DATE).toString());
    }

    /**
     * Method: parseDate(String strDate)
     */
    @Test
    public void testParseDate() throws Exception {
        System.out.println(DateUtil.parseDate(DateUtil.getNowDate()).toString());
    }

    /**
     * Method: parseTime(String strDate)
     */
    @Test
    public void testParseTime() throws Exception {
        System.out.println(DateUtil.parseTime(DateUtil.getNowTime()).toString());
    }

    /**
     * Method: parseDateTime(String strDate)
     */
    @Test
    public void testParseDateTime() throws Exception {
        System.out.println(DateUtil.parseDateTime(DateUtil.getNowDateTime()).toString());
    }

    /**
     * Method: parseTimestamp(String strDate)
     */
    @Test
    public void testParseTimestamp() throws Exception {
        System.out.println(DateUtil.parseTimestamp(DateUtil.getNowTimestamp()).toString());
    }

    /**
     * Method: date2Calendar(Date date)
     */
    @Test
    public void testDate2Calendar() throws Exception {
        System.out.println(DateUtil.date2Calendar(DateUtil.getNow()).toString());
    }

    /**
     * Method: dateAdd(Date date, int field, int amount)
     */
    @Test
    public void testDateAdd() throws Exception {
        System.out.println(DateUtil.dateAdd(DateUtil.getNow(), Calendar.DATE, 1).toString());
    }

    @Test
    public void getWeekFirstDay() {
        System.out.println(DateUtil.formatDate(DateUtil.getWeekFirstDay(DateUtil.getNow())));
    }

    @Test
    public void getWeekLastDay() {
        System.out.println(DateUtil.formatDate(DateUtil.getWeekLastDay(DateUtil.getNow())));
    }
}
