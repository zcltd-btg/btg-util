package cn.zcltd.btg.sutil;

import org.junit.Test;

import static cn.zcltd.btg.sutil.UrlUtil.parse;

/**
 * UrlUtil Tester.
 */
public class UrlUtilTest {

    /**
     * Method: parse(String url)
     */
    @Test
    public void testParse() throws Exception {
        UrlUtil.UrlEntity entity = UrlUtil.parse(null);
        System.out.println(entity.getBaseUrl() + "\n" + entity.getParams());
        entity = parse("http://www.123.com");
        System.out.println(entity.getBaseUrl() + "\n" + entity.getParams());
        entity = parse("http://www.123.com?id=1");
        System.out.println(entity.getBaseUrl() + "\n" + entity.getParams());
        entity = parse("http://www.123.com?id=1&name=小明");
        System.out.println(entity.getBaseUrl() + "\n" + entity.getParams());
    }
}
