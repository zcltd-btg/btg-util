package cn.zcltd.btg.sutil.filescanner;

import cn.zcltd.btg.sutil.FileUtil;
import cn.zcltd.btg.sutil.PathUtil;
import org.junit.Test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * FileScanner Tester.
 */
public class FileScannerTest {
    /**
     * Method: findDirs(String baseDirPath, String matchesStr)
     */
    @Test
    public void testFindDirsForBaseDirPathMatchesStr() {
        List<java.io.File> dirs = FileScanner.findDirs(PathUtil.getRootClassPath(), ".*");
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
    }

    /**
     * Method: findDirs(String matchesStr)
     */
    @Test
    public void testFindDirsMatchesStr() {
        List<java.io.File> dirs = FileScanner.findDirs(".*");
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
    }

    /**
     * Method: findDirs()
     */
    @Test
    public void testFindDirs() {
        List<java.io.File> dirs = FileScanner.findDirs();
        for (java.io.File dir : dirs) {
            System.out.println(dir.getPath());
        }
    }

    /**
     * Method: findFiles(String baseDirPath, String matchesStr, boolean scanChildDir)
     */
    @Test
    public void testFindFilesForBaseDirPathMatchesStrScanChildDir() {
        List<java.io.File> files = FileScanner.findFiles(PathUtil.getRootClassPath(), ".*", true);
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFiles(String baseDirPath, String matchesStr)
     */
    @Test
    public void testFindFilesForBaseDirPathMatchesStr() {
        List<java.io.File> files = FileScanner.findFiles(PathUtil.getRootClassPath(), ".*");
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFiles(String matchesStr)
     */
    @Test
    public void testFindFilesMatchesStr() {
        List<java.io.File> files = FileScanner.findFiles(".*");
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFiles()
     */
    @Test
    public void testFindFiles() {
        List<java.io.File> files = FileScanner.findFiles();
        for (java.io.File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFilesFromJar(java.io.File fileJar, String matchesStr4Package, String matchesStr4File)
     */
    @Test
    public void testFindFilesFromJarForFileJarMatchesStr4PackageMatchesStr4File() {
        List<File> files = FileScanner.findFilesFromJar(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"), ".*", "A.*");
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFilesFromJar(java.io.File fileJar, String matchesStr4File)
     */
    @Test
    public void testFindFilesFromJarForFileJarMatchesStr4File() {
        List<File> files = FileScanner.findFilesFromJar(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"), "A.*");
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFilesFromJar(java.io.File fileJar)
     */
    @Test
    public void testFindFilesFromJarFileJar() {
        List<File> files = FileScanner.findFilesFromJar(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"));
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFilesFromJars(List<java.io.File> filesJar, String matchesStr4Package, String matchesStr4File)
     */
    @Test
    public void testFindFilesFromJarsForFilesJarMatchesStr4PackageMatchesStr4File() {
        List<java.io.File> fileJars = new ArrayList<java.io.File>();
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"));
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\log4j\\log4j\\1.2.12\\log4j-1.2.12.jar"));
        List<File> files = FileScanner.findFilesFromJars(fileJars, "org.junit.*", ".*");
        for (File file : files) {
            System.out.println(file.toString());
            InputStream is = this.getClass().getResourceAsStream(file.getPath());
            System.out.println(FileUtil.readToString(is));
        }
    }

    /**
     * Method: findFilesFromJars(List<java.io.File> filesJar, String matchesStr4File)
     */
    @Test
    public void testFindFilesFromJarsForFilesJarMatchesStr4File() {
        List<java.io.File> fileJars = new ArrayList<java.io.File>();
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"));
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\log4j\\log4j\\1.2.12\\log4j-1.2.12.jar"));
        List<File> files = FileScanner.findFilesFromJars(fileJars, ".*");
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findFilesFromJars(List<java.io.File> filesJar)
     */
    @Test
    public void testFindFilesFromJarsFilesJar() {
        List<java.io.File> fileJars = new ArrayList<java.io.File>();
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"));
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\log4j\\log4j\\1.2.12\\log4j-1.2.12.jar"));
        List<File> files = FileScanner.findFilesFromJars(fileJars);
        for (File file : files) {
            System.out.println(file.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, boolean scanChildDir, String pre, String matchesStr4Package, String matchesStr4File, Class parentClass, boolean parentClassIsInstanceOf)
     */
    @Test
    public void testFindClassForBaseDirPathScanChildDirPreMatchesStr4PackageMatchesStr4FileParentClassParentClassIsInstanceOf() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), true, PathUtil.getRootClassPath(), ".*", ".*", Object.class, true);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, boolean scanChildDir, String pre, String matchesStr4File, Class parentClass, boolean parentClassIsInstanceOf)
     */
    @Test
    public void testFindClassForBaseDirPathScanChildDirPreMatchesStr4FileParentClassParentClassIsInstanceOf() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), true, PathUtil.getRootClassPath(), ".*", Object.class, true);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, boolean scanChildDir, String pre, String matchesStr4File, Class parentClass)
     */
    @Test
    public void testFindClassForBaseDirPathScanChildDirPreMatchesStr4FileParentClass() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), true, PathUtil.getRootClassPath(), ".*", Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, boolean scanChildDir, String pre, String matchesStr4File)
     */
    @Test
    public void testFindClassForBaseDirPathScanChildDirPreMatchesStr4File() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), true, PathUtil.getRootClassPath(), ".*");
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, String pre, String matchesStr4File, Class parentClass)
     */
    @Test
    public void testFindClassForBaseDirPathPreMatchesStr4FileParentClass() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), PathUtil.getRootClassPath(), ".*", Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, String pre, String matchesStr4File)
     */
    @Test
    public void testFindClassForBaseDirPathPreMatchesStr4File() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), PathUtil.getRootClassPath(), ".*");
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, String pre, Class parentClass)
     */
    @Test
    public void testFindClassForBaseDirPathPreParentClass() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), PathUtil.getRootClassPath(), Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String baseDirPath, String pre)
     */
    @Test
    public void testFindClassForBaseDirPathPre() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), PathUtil.getRootClassPath());
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String pre, Class parentClass)
     */
    @Test
    public void testFindClassForPreParentClass() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath(), Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClass(String pre)
     */
    @Test
    public void testFindClassPre() throws Exception {
        List<Class> classes = FileScanner.findClass(PathUtil.getRootClassPath());
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJar(java.io.File fileJar, String matchesStr4Package, String matchesStr4File, Class parentClass, boolean parentClassIsInstanceOf)
     */
    @Test
    public void testFindClassFromJarForFileJarMatchesStr4PackageMatchesStr4FileParentClassParentClassIsInstanceOf() throws Exception {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"), ".*", ".*", Object.class, true);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJar(java.io.File fileJar, String matchesStr4Package, String matchesStr4File, Class parentClass)
     */
    @Test
    public void testFindClassFromJarForFileJarMatchesStr4PackageMatchesStr4FileParentClass() throws Exception {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"), ".*", ".*", Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJar(java.io.File fileJar, String matchesStr4File, Class parentClass)
     */
    @Test
    public void testFindClassFromJarForFileJarMatchesStr4FileParentClass() throws Exception {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"), ".*", Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJar(java.io.File fileJar, Class parentClass)
     */
    @Test
    public void testFindClassFromJarForFileJarParentClass() throws Exception {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"), Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJar(java.io.File fileJar)
     */
    @Test
    public void testFindClassFromJarFileJar() throws Exception {
        List<Class> classes = FileScanner.findClassFromJar(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"));
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJars(List<java.io.File> filesJar, String matchesStr4Package, String matchesStr4File, Class parentClass, boolean parentClassIsInstanceOf)
     */
    @Test
    public void testFindClassFromJarsForFilesJarMatchesStr4PackageMatchesStr4FileParentClassParentClassIsInstanceOf() throws Exception {
        List<java.io.File> fileJars = new ArrayList<java.io.File>();
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"));
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\log4j\\log4j\\1.2.12\\log4j-1.2.12.jar"));
        List<Class> classes = FileScanner.findClassFromJars(fileJars, ".*", ".*", Object.class, true);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJars(List<java.io.File> filesJar, String matchesStr4Package, String matchesStr4File, Class parentClass)
     */
    @Test
    public void testFindClassFromJarsForFilesJarMatchesStr4PackageMatchesStr4FileParentClass() throws Exception {
        List<java.io.File> fileJars = new ArrayList<java.io.File>();
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"));
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\log4j\\log4j\\1.2.12\\log4j-1.2.12.jar"));
        List<Class> classes = FileScanner.findClassFromJars(fileJars, ".*", ".*", Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJars(List<java.io.File> filesJar, String matchesStr4File, Class parentClass)
     */
    @Test
    public void testFindClassFromJarsForFilesJarMatchesStr4FileParentClass() throws Exception {
        List<java.io.File> fileJars = new ArrayList<java.io.File>();
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"));
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\log4j\\log4j\\1.2.12\\log4j-1.2.12.jar"));
        List<Class> classes = FileScanner.findClassFromJars(fileJars, ".*", Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJars(List<java.io.File> filesJar, Class parentClass)
     */
    @Test
    public void testFindClassFromJarsForFilesJarParentClass() throws Exception {
        List<java.io.File> fileJars = new ArrayList<java.io.File>();
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"));
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\log4j\\log4j\\1.2.12\\log4j-1.2.12.jar"));
        List<Class> classes = FileScanner.findClassFromJars(fileJars, Object.class);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }

    /**
     * Method: findClassFromJars(List<java.io.File> filesJar)
     */
    @Test
    public void testFindClassFromJarsFilesJar() throws Exception {
        List<java.io.File> fileJars = new ArrayList<java.io.File>();
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\junit\\junit\\4.12\\junit-4.12.jar"));
        fileJars.add(new java.io.File("D:\\resource\\maven_repository\\log4j\\log4j\\1.2.12\\log4j-1.2.12.jar"));
        List<Class> classes = FileScanner.findClassFromJars(fileJars);
        for (Class cls : classes) {
            System.out.println(cls.toString());
        }
    }
}