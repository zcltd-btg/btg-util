package cn.zcltd.btg.sutil.unicode;

import org.junit.Test;

/**
 * NotAsciiUnicodeConverter Tester.
 */
public class NotAsciiUnicodeConverterTest {
    /**
     * Method: encrypt(String jsonStr)
     */
    @Test
    public void testEncrypt() throws Exception {
        String str = "{\"qd_infos\":[{\"qd_params\":[{\"param_value\":\"公司五六\",\"param_bm\":\"CCB_ZHM\",\"param_mc\":\"账户名\"}],\"qd_mc_s\":\"建行\",\"qd_mc\":\"建设银行-银企直联\",\"qd_lx_mc\":\"银企直联-建设银行\",\"qd_desc\":\"建设银行-银企直联\",\"qd_bm\":\"BANK_CCB_01\",\"qd_lx_bm\":\"BANK_CCB\"}],\"resp\":{\"timestamp\":\"2017-10-31 14:42:32\",\"resp_desc\":\"接口调用成功(^_^)\",\"biz_code\":\"00\",\"resp_code\":\"00\",\"biz_desc\":\"请求成功(^_^)\"}}";
        //System.out.println(NotAsciiUnicodeConverter.me.encrypt(str));
    }

    /**
     * Method: decrypt(String unicodeJsonStr)
     */
    @Test
    public void testDecrypt() throws Exception {
        String str = "{\"qd_infos\":[{\"qd_params\":[{\"param_value\":\"\\u516c\\u53f8\\u4e94\\u516d\",\"param_bm\":\"CCB_ZHM\",\"param_mc\":\"\\u8d26\\u6237\\u540d\"}],\"qd_mc_s\":\"\\u5efa\\u884c\",\"qd_mc\":\"\\u5efa\\u8bbe\\u94f6\\u884c-\\u94f6\\u4f01\\u76f4\\u8054\",\"qd_lx_mc\":\"\\u94f6\\u4f01\\u76f4\\u8054-\\u5efa\\u8bbe\\u94f6\\u884c\",\"qd_desc\":\"\\u5efa\\u8bbe\\u94f6\\u884c-\\u94f6\\u4f01\\u76f4\\u8054\",\"qd_bm\":\"BANK_CCB_01\",\"qd_lx_bm\":\"BANK_CCB\"}],\"resp\":{\"timestamp\":\"2017-10-31\\u002014:42:32\",\"resp_desc\":\"\\u63a5\\u53e3\\u8c03\\u7528\\u6210\\u529f(^_^)\",\"biz_code\":\"00\",\"resp_code\":\"00\",\"biz_desc\":\"\\u8bf7\\u6c42\\u6210\\u529f(^_^)\"}}";
        //System.out.println(NotAsciiUnicodeConverter.me.decrypt(str));
    }
}