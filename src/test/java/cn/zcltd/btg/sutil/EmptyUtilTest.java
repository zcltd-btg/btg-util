package cn.zcltd.btg.sutil;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

/**
 * EmptyUtil Tester.
 */
public class EmptyUtilTest {
    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    /**
     * Method: isEmpty(Object obj)
     */
    @Test
    public void testIsEmptyObj() throws Exception {
        Object target = null;
        System.out.println(EmptyUtil.isEmpty(target));

        Object target2 = new Object();
        System.out.println(EmptyUtil.isEmpty(target2));
    }

    /**
     * Method: isNotEmpty(Object obj)
     */
    @Test
    public void testIsNotEmptyObj() throws Exception {
        Object target = null;
        System.out.println(EmptyUtil.isNotEmpty(target));

        Object target2 = new Object();
        System.out.println(EmptyUtil.isNotEmpty(target2));
    }

    /**
     * Method: isEmpty(List list)
     */
    @Test
    public void testIsEmptyList() throws Exception {
        List target = null;
        System.out.println(EmptyUtil.isEmpty(target));

        List target2 = new ArrayList();
        System.out.println(EmptyUtil.isEmpty(target2));

        List target3 = new ArrayList();
        target3.add("1");
        System.out.println(EmptyUtil.isEmpty(target3));
    }

    /**
     * Method: isNotEmpty(List list)
     */
    @Test
    public void testIsNotEmptyList() throws Exception {
        Object target = null;
        System.out.println(EmptyUtil.isNotEmpty(target));

        List target2 = new ArrayList();
        System.out.println(EmptyUtil.isNotEmpty(target2));

        List target3 = new ArrayList();
        target3.add("1");
        System.out.println(EmptyUtil.isNotEmpty(target3));
    }

    /**
     * Method: isEmpty(Map map)
     */
    @Test
    public void testIsEmptyMap() throws Exception {
        Map target = null;
        System.out.println(EmptyUtil.isEmpty(target));

        Map target2 = new HashMap();
        System.out.println(EmptyUtil.isEmpty(target2));

        Map target3 = new HashMap();
        target3.put("1", "1");
        System.out.println(EmptyUtil.isEmpty(target3));
    }

    /**
     * Method: isNotEmpty(Map map)
     */
    @Test
    public void testIsNotEmptyMap() throws Exception {
        Map target = null;
        System.out.println(EmptyUtil.isNotEmpty(target));

        Map target2 = new HashMap();
        System.out.println(EmptyUtil.isNotEmpty(target2));

        Map target3 = new HashMap();
        target3.put("1", "1");
        System.out.println(EmptyUtil.isNotEmpty(target3));
    }

    /**
     * Method: isEmpty(Set set)
     */
    @Test
    public void testIsEmptySet() throws Exception {
        Set target = null;
        System.out.println(EmptyUtil.isEmpty(target));

        Set target2 = new HashSet();
        System.out.println(EmptyUtil.isEmpty(target2));

        Set target3 = new HashSet();
        target3.add("1");
        System.out.println(EmptyUtil.isEmpty(target3));
    }

    /**
     * Method: isNotEmpty(Set set)
     */
    @Test
    public void testIsNotEmptySet() throws Exception {
        Set target = null;
        System.out.println(EmptyUtil.isNotEmpty(target));

        Set target2 = new HashSet();
        System.out.println(EmptyUtil.isNotEmpty(target2));

        Set target3 = new HashSet();
        target3.add("1");
        System.out.println(EmptyUtil.isNotEmpty(target3));
    }

    /**
     * Method: isEmpty(String str)
     */
    @Test
    public void testIsEmptyStr() throws Exception {
        String target = null;
        System.out.println(EmptyUtil.isEmpty(target));

        String target2 = "";
        System.out.println(EmptyUtil.isEmpty(target2));

        String target3 = "333";
        System.out.println(EmptyUtil.isEmpty(target3));
    }

    /**
     * Method: isNotEmpty(String str)
     */
    @Test
    public void testIsNotEmptyStr() throws Exception {
        String target = null;
        System.out.println(EmptyUtil.isNotEmpty(target));

        String target2 = "";
        System.out.println(EmptyUtil.isNotEmpty(target2));

        String target3 = "333";
        System.out.println(EmptyUtil.isNotEmpty(target3));
    }

    /**
     * Method: isEmpty(StringBuilder str)
     */
    @Test
    public void testIsEmptyStringBuilder() throws Exception {
        StringBuilder target = new StringBuilder();
        System.out.println(EmptyUtil.isEmpty(target));

        StringBuilder target2 = new StringBuilder("");
        System.out.println(EmptyUtil.isEmpty(target2));

        StringBuilder target3 = new StringBuilder("333");
        System.out.println(EmptyUtil.isEmpty(target3));
    }

    /**
     * Method: isEmpty(StringBuilder str)
     */
    @Test
    public void testIsNotEmptyStringBuilder() throws Exception {
        StringBuilder target = new StringBuilder();
        System.out.println(EmptyUtil.isNotEmpty(target));

        StringBuilder target2 = new StringBuilder("");
        System.out.println(EmptyUtil.isNotEmpty(target2));

        StringBuilder target3 = new StringBuilder("333");
        System.out.println(EmptyUtil.isNotEmpty(target3));
    }

    /**
     * Method: isEmpty(StringBuffer str)
     */
    @Test
    public void testIsEmptyStringBuffer() throws Exception {
        StringBuffer target = new StringBuffer();
        System.out.println(EmptyUtil.isEmpty(target));

        StringBuffer target2 = new StringBuffer("");
        System.out.println(EmptyUtil.isEmpty(target2));

        StringBuffer target3 = new StringBuffer("333");
        System.out.println(EmptyUtil.isEmpty(target3));
    }

    /**
     * Method: isEmpty(StringBuffer str)
     */
    @Test
    public void testIsNotEmptyStringBuffer() throws Exception {
        StringBuffer target = new StringBuffer();
        System.out.println(EmptyUtil.isNotEmpty(target));

        StringBuffer target2 = new StringBuffer("");
        System.out.println(EmptyUtil.isNotEmpty(target2));

        StringBuffer target3 = new StringBuffer("333");
        System.out.println(EmptyUtil.isNotEmpty(target3));
    }

    /**
     * Method: isEmpty(T[] array)
     */
    @Test
    public void testIsEmptyArray() throws Exception {
        String[] array = new String[0];
        System.out.println(EmptyUtil.isEmpty(array));

        String[] array2 = new String[1];
        System.out.println(EmptyUtil.isEmpty(array2));
    }

    /**
     * Method: isNotEmpty(T[] array)
     */
    @Test
    public void testIsNotEmptyArray() throws Exception {
        String[] array = new String[0];
        System.out.println(EmptyUtil.isNotEmpty(array));

        String[] array2 = new String[1];
        System.out.println(EmptyUtil.isNotEmpty(array2));
    }

    /**
     * Method: isEmpty(byte[] bytes)
     */
    @Test
    public void testIsEmptyBytes() throws Exception {
        byte[] bytes = "".getBytes();
        System.out.println(EmptyUtil.isEmpty(bytes));

        byte[] bytes2 = "aaa".getBytes();
        System.out.println(EmptyUtil.isEmpty(bytes2));
    }

    /**
     * Method: isNotEmpty(byte[] bytes)
     */
    @Test
    public void testIsNotEmptyBytes() throws Exception {
        byte[] bytes = "".getBytes();
        System.out.println(EmptyUtil.isNotEmpty(bytes));

        byte[] bytes2 = "aaa".getBytes();
        System.out.println(EmptyUtil.isNotEmpty(bytes2));
    }
}
