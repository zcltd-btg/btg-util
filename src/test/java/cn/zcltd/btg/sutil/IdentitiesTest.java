package cn.zcltd.btg.sutil;

import org.junit.Test;

/**
 * Identities Tester.
 */
public class IdentitiesTest {
    /**
     * Method: random(int min, int max)
     */
    @Test
    public void testRandomForMinMax() throws Exception {
        System.out.println(Identities.random(3, 9));
    }

    /**
     * Method: random(int length)
     */
    @Test
    public void testRandomLength() throws Exception {
        System.out.println(Identities.random(9));
    }

    /**
     * Method: uuidOriginal()
     */
    @Test
    public void testUuidOriginal() throws Exception {
        System.out.println(Identities.uuidOriginal());
    }

    /**
     * Method: uuid()
     */
    @Test
    public void testUuid() throws Exception {
        System.out.println(Identities.uuid());
    }

    /**
     * Method: serialNo(int timestampBeginIndex, int timestampEndIndex, int randomLength)
     */
    @Test
    public void testSerialNoForTimestampBeginIndexTimestampEndIndexRandomLength() throws Exception {
        System.out.println(Identities.serialNo(0, 4, 3));
    }

    /**
     * Method: serialNo()
     */
    @Test
    public void testSerialNo() throws Exception {
        System.out.println(Identities.serialNo());
    }

}
