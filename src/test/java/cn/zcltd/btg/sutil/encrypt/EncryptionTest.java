package cn.zcltd.btg.sutil.encrypt;

import org.junit.Test;

/**
 * Encryption Tester.
 */
public class EncryptionTest {

    @Test
    public void test() throws Exception {
        String sourceStr = "hello 世界!";
        byte[] sourceBytes = sourceStr.getBytes(Encryption.ENCODING_UTF8);
        String mykeyStr = "mykey";
        byte[] mykeyBytes = mykeyStr.getBytes(Encryption.ENCODING_UTF8);
        String keyStr;
        byte[] keyBytes;
        String ivStr = "12345678";
        byte[] ivBytes = ivStr.getBytes(Encryption.ENCODING_UTF8);
        String ivStrL = "1234567812345678";
        byte[] ivBytesL = ivStrL.getBytes(Encryption.ENCODING_UTF8);
        byte[] encodeBytes;
        String encodeStr;
        byte[] decodeBytes;
        String decodeStr;

        //原始数据
        System.out.println("原始数据：\t" + sourceStr);
        System.out.println("----------------------------------------");

        /*
            BASE64加密解密
        */
        //BASE64加密
        encodeStr = Encryption.encryptBASE64ToString(sourceBytes);
        System.out.println("BASE64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptBASE64ToString(sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("BASE64加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptBASE64ToString(sourceStr);
        System.out.println("BASE64加密后3：\t" + encodeStr);

        //BASE64解密
        decodeBytes = Encryption.decryptBASE64(encodeStr);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("BASE64解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptBASE64ToString(encodeStr, Encryption.ENCODING_UTF8);
        System.out.println("BASE64解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptBASE64ToString(encodeStr);
        System.out.println("BASE64解密后3：\t" + decodeStr);

        System.out.println("----------------------------------------");

        /*
            MD5加密
        */
        encodeBytes = Encryption.encryptMD5(sourceBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("MD5-hex加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptMD5ToHex(sourceBytes);
        System.out.println("MD5-hex加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptMD5ToHex(sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("MD5-hex加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptMD5ToHex(sourceStr);
        System.out.println("MD5-hex加密后4：\t" + encodeStr);

        encodeStr = Encryption.encryptBASE64ToString(encodeBytes);
        System.out.println("MD5-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptMD5ToBase64(sourceBytes);
        System.out.println("MD5-base64加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptMD5ToBase64(sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("MD5-base64加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptMD5ToBase64(sourceStr);
        System.out.println("MD5-base64加密后4：\t" + encodeStr);

        System.out.println("----------------------------------------");

        /*
            SHA加密
        */
        encodeBytes = Encryption.encryptSHA(Encryption.ALGORITHM_SHA_BIT_1, sourceBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("SHA-hex加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptSHAToHex(Encryption.ALGORITHM_SHA_BIT_1, sourceBytes);
        System.out.println("SHA-hex加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptSHAToHex(Encryption.ALGORITHM_SHA_BIT_1, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("SHA-hex加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptSHAToHex(Encryption.ALGORITHM_SHA_BIT_1, sourceStr);
        System.out.println("SHA-hex加密后4：\t" + encodeStr);

        encodeStr = Encryption.encryptBASE64ToString(encodeBytes);
        System.out.println("SHA-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptSHAToBase64(Encryption.ALGORITHM_SHA_BIT_1, sourceBytes);
        System.out.println("SHA-base64加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptSHAToBase64(Encryption.ALGORITHM_SHA_BIT_1, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("SHA-base64加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptSHAToBase64(Encryption.ALGORITHM_SHA_BIT_1, sourceStr);
        System.out.println("SHA-base64加密后4：\t" + encodeStr);

        System.out.println("----------------------------------------");

        /*
            HMAC带秘钥加密
        */
        Encryption.EncryptionKey hmacKey = Encryption.generatorHMACKey(1, Encryption.HAMC_ALGORITHM_MD5, mykeyBytes);
        keyBytes = hmacKey.getKey();
        System.out.println("HMAC-hex秘钥：\t" + hmacKey.getKeyHex());
        System.out.println("HMAC-base64秘钥：\t" + hmacKey.getKeyBase64());

        encodeBytes = Encryption.encryptHMAC(Encryption.HAMC_ALGORITHM_MD5, sourceBytes, keyBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("HMAC-hex后1：\t" + encodeStr);

        encodeStr = Encryption.encryptHMACToHex(Encryption.HAMC_ALGORITHM_MD5, sourceBytes, keyBytes);
        System.out.println("HMAC-hex后2：\t" + encodeStr);

        encodeStr = Encryption.encryptHMACToHex(Encryption.HAMC_ALGORITHM_MD5, sourceStr, keyBytes, Encryption.ENCODING_UTF8);
        System.out.println("HMAC-hex后3：\t" + encodeStr);

        encodeStr = Encryption.encryptHMACToHex(Encryption.HAMC_ALGORITHM_MD5, sourceStr, keyBytes);
        System.out.println("HMAC-hex后4：\t" + encodeStr);

        encodeBytes = Encryption.encryptHMAC(Encryption.HAMC_ALGORITHM_MD5, sourceBytes, keyBytes);
        encodeStr = Encryption.encryptBASE64ToString(encodeBytes);
        System.out.println("HMAC-base64后1：\t" + encodeStr);

        encodeStr = Encryption.encryptHMACToBase64(Encryption.HAMC_ALGORITHM_MD5, sourceBytes, keyBytes);
        System.out.println("HMAC-base64后2：\t" + encodeStr);

        encodeStr = Encryption.encryptHMACToBase64(Encryption.HAMC_ALGORITHM_MD5, sourceStr, keyBytes, Encryption.ENCODING_UTF8);
        System.out.println("HMAC-base64后3：\t" + encodeStr);

        encodeStr = Encryption.encryptHMACToBase64(Encryption.HAMC_ALGORITHM_MD5, sourceStr, keyBytes);
        System.out.println("HMAC-base64后4：\t" + encodeStr);

        System.out.println("----------------------------------------");

        /*
          DES带秘钥加密解密
        */
        Encryption.EncryptionKey desKey = Encryption.generatorDESKey(56, mykeyBytes);
        keyBytes = desKey.getKey();
        System.out.println("DES-hex秘钥：\t" + desKey.getKeyHex());
        System.out.println("DES-base64秘钥：\t" + desKey.getKeyBase64());
        System.out.println("DES-iv：\t" + ivStr);

        //加密-hex
        encodeBytes = Encryption.encryptDES(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("DES-hex加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptDESToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytes);
        System.out.println("DES-hex加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptDESToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES-hex加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptDESToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes);
        System.out.println("DES-hex加密后4：\t" + encodeStr);

        //解密
        decodeBytes = Encryption.decryptDES(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeBytes, keyBytes, ivBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES解密后1：\t" + decodeStr);

        //解密-hex
        decodeBytes = Encryption.decryptDESFromHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES-hex解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptDESFromHexToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DES-hex解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptDESFromHexToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES-hex解密后3：\t" + decodeStr);

        decodeStr = Encryption.decryptDESFromHexToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DES-hex解密后4：\t" + decodeStr);

        //加密-base64
        encodeBytes = Encryption.encryptDES(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytes);
        encodeStr = Encryption.encryptBASE64ToString(encodeBytes);
        System.out.println("DES-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptDESToBase64(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytes);
        System.out.println("DES-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptDESToBase64(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES-base64加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptDESToBase64(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes);
        System.out.println("DES-base64加密后3：\t" + encodeStr);

        //解密-base64
        decodeBytes = Encryption.decryptDESFromBase64(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES-base64解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptDESFromBase64ToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DES-base64解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptDESFromBase64ToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DES-base64解密后3：\t" + decodeStr);

        decodeStr = Encryption.decryptDESFromBase64ToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DES-base64解密后4：\t" + decodeStr);

        System.out.println("----------------------------------------");

        /*
          DESede带秘钥加密解密
        */
        Encryption.EncryptionKey deSedeKey = Encryption.generatorDESedeKey(112, mykeyBytes);
        keyBytes = deSedeKey.getKey();
        System.out.println("DESede-hex秘钥：\t" + deSedeKey.getKeyHex());
        System.out.println("DESede-base64秘钥：\t" + deSedeKey.getKeyBase64());
        System.out.println("DESede-iv：\t" + ivStr);

        //加密-hex
        encodeBytes = Encryption.encryptDESede(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("DESede-hex加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptDESedeToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytes);
        System.out.println("DESede-hex加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptDESedeToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede-hex加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptDESedeToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes);
        System.out.println("DESede-hex加密后4：\t" + encodeStr);

        //解密
        decodeBytes = Encryption.decryptDESede(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeBytes, keyBytes, ivBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede解密后1：\t" + decodeStr);

        //解密-hex
        decodeBytes = Encryption.decryptDESedeFromHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede-hex解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptDESedeFromHexToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DESede-hex解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptDESedeFromHexToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede-hex解密后3：\t" + decodeStr);

        decodeStr = Encryption.decryptDESedeFromHexToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DESede-hex解密后4：\t" + decodeStr);

        //加密-base64
        encodeBytes = Encryption.encryptDESede(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytes);
        encodeStr = Encryption.encryptBASE64ToString(encodeBytes);
        System.out.println("DESede-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptDESedeToBase64(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytes);
        System.out.println("DESede-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptDESedeToBase64(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede-base64加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptDESedeToBase64(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytes);
        System.out.println("DESede-base64加密后3：\t" + encodeStr);

        //解密-base64
        decodeBytes = Encryption.decryptDESedeFromBase64(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede-base64解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptDESedeFromBase64ToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DESede-base64解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptDESedeFromBase64ToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes, Encryption.ENCODING_UTF8);
        System.out.println("DESede-base64解密后3：\t" + decodeStr);

        decodeStr = Encryption.decryptDESedeFromBase64ToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytes);
        System.out.println("DESede-base64解密后4：\t" + decodeStr);

        System.out.println("----------------------------------------");

        /*
          AES带秘钥加密解密
        */
        Encryption.EncryptionKey aesKey = Encryption.generatorAESKey(128);
        keyBytes = aesKey.getKey();
        System.out.println("AES-hex秘钥：\t" + aesKey.getKeyHex());
        System.out.println("AES-base64秘钥：\t" + aesKey.getKeyBase64());
        System.out.println("AES-iv：\t" + ivStrL);

        //加密-hex
        encodeBytes = Encryption.encryptAES(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytesL);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("AES-hex加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptAESToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytesL);
        System.out.println("AES-hex加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptAESToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytesL, Encryption.ENCODING_UTF8);
        System.out.println("AES-hex加密后3：\t" + encodeStr);

        encodeStr = Encryption.encryptAESToHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytesL);
        System.out.println("AES-hex加密后4：\t" + encodeStr);

        //解密
        decodeBytes = Encryption.decryptAES(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeBytes, keyBytes, ivBytesL);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("AES解密后1：\t" + decodeStr);

        //解密-hex
        decodeBytes = Encryption.decryptAESFromHex(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("AES-hex解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptAESFromHexToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL);
        System.out.println("AES-hex解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptAESFromHexToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL, Encryption.ENCODING_UTF8);
        System.out.println("AES-hex解密后3：\t" + decodeStr);

        decodeStr = Encryption.decryptAESFromHexToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL);
        System.out.println("AES-hex解密后4：\t" + decodeStr);

        //加密-base64
        encodeBytes = Encryption.encryptAES(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytesL);
        encodeStr = Encryption.encryptBASE64ToString(encodeBytes);
        System.out.println("AES-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptAESToBase64(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceBytes, keyBytes, ivBytesL);
        System.out.println("AES-base64加密后1：\t" + encodeStr);

        encodeStr = Encryption.encryptAESToBase64(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytesL, Encryption.ENCODING_UTF8);
        System.out.println("AES-base64加密后2：\t" + encodeStr);

        encodeStr = Encryption.encryptAESToBase64(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, sourceStr, keyBytes, ivBytesL);
        System.out.println("AES-base64加密后3：\t" + encodeStr);

        //解密-base64
        decodeBytes = Encryption.decryptAESFromBase64(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("AES-base64解密后1：\t" + decodeStr);

        decodeStr = Encryption.decryptAESFromBase64ToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL);
        System.out.println("AES-base64解密后2：\t" + decodeStr);

        decodeStr = Encryption.decryptAESFromBase64ToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL, Encryption.ENCODING_UTF8);
        System.out.println("AES-base64解密后3：\t" + decodeStr);

        decodeStr = Encryption.decryptAESFromBase64ToString(Encryption.ENCRYPT_MODE_CBC, Encryption.ENCRYPT_PADDING_PKCS5PADDING, encodeStr, keyBytes, ivBytesL);
        System.out.println("AES-base64解密后4：\t" + decodeStr);
    }
}