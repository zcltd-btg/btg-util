package cn.zcltd.btg.sutil.encrypt;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

/**
 * EncryptionRSA Tester.
 */
public class EncryptionRSATest {

    @Before
    public void before() throws Exception {

    }

    @After
    public void after() throws Exception {

    }

    /**
     * Method: generatorRSAKey(int keySize, byte[] seed)
     */
    @Test
    public void testGeneratorRSAKeyForKeySizeSeed() throws Exception {
        EncryptionRSA.EncryptionKeyRSA encryptionKeyRSA = EncryptionRSA.generatorRSAKey(1024, "mykey".getBytes());

        System.out.println(encryptionKeyRSA.getModulus().toString());
        System.out.println(encryptionKeyRSA.getModulusHex());
        System.out.println(encryptionKeyRSA.getPublicExponent().toString());
        System.out.println(encryptionKeyRSA.getPublicExponentHex());
        System.out.println(encryptionKeyRSA.getPrivateExponent().toString());
        System.out.println(encryptionKeyRSA.getPrivateExponentHex());
        System.out.println(encryptionKeyRSA.getPublicKeyHex());
        System.out.println(encryptionKeyRSA.getPublicKeyBase64());
        System.out.println(encryptionKeyRSA.getPrivateKeyHex());
        System.out.println(encryptionKeyRSA.getPrivateKeyBase64());
    }

    /**
     * Method: generatorRSAKey(int keySize)
     */
    @Test
    public void testGeneratorRSAKeyKeySize() throws Exception {
        EncryptionRSA.EncryptionKeyRSA encryptionKeyRSA = EncryptionRSA.generatorRSAKey(1024);

        System.out.println(encryptionKeyRSA.getModulus().toString());
        System.out.println(encryptionKeyRSA.getModulusHex());
        System.out.println(encryptionKeyRSA.getPublicExponent().toString());
        System.out.println(encryptionKeyRSA.getPublicExponentHex());
        System.out.println(encryptionKeyRSA.getPrivateExponent().toString());
        System.out.println(encryptionKeyRSA.getPrivateExponentHex());
        System.out.println(encryptionKeyRSA.getPublicKeyHex());
        System.out.println(encryptionKeyRSA.getPublicKeyBase64());
        System.out.println(encryptionKeyRSA.getPrivateKeyHex());
        System.out.println(encryptionKeyRSA.getPrivateKeyBase64());
    }

    /**
     * Method: parseRSAPublicKey(BigInteger modulus, BigInteger exponent)
     */
    @Test
    public void testParseRSAPublicKey() throws Exception {
        EncryptionRSA.EncryptionKeyRSA encryptionKeyRSA = EncryptionRSA.generatorRSAKey(1024, "mykey".getBytes());
        RSAPublicKey publicKey = EncryptionRSA.parseRSAPublicKey(encryptionKeyRSA.getModulus(), encryptionKeyRSA.getPublicExponent());
        System.out.println(publicKey.toString());
    }

    /**
     * Method: parseRSAPublicKeyFromHex(String publicKeyHex)
     */
    @Test
    public void testParseRSAPublicKeyFromHex() throws Exception {
        EncryptionRSA.EncryptionKeyRSA encryptionKeyRSA = EncryptionRSA.generatorRSAKey(1024, "mykey".getBytes());
        RSAPublicKey publicKey = EncryptionRSA.parseRSAPublicKeyFromHex(encryptionKeyRSA.getPublicKeyHex());
        System.out.println(publicKey.toString());
    }

    /**
     * Method: parseRSAPublicKeyFromBase64(String publicKeyBase64)
     */
    @Test
    public void testParseRSAPublicKeyFromBase64() throws Exception {
        EncryptionRSA.EncryptionKeyRSA encryptionKeyRSA = EncryptionRSA.generatorRSAKey(1024, "mykey".getBytes());
        RSAPublicKey publicKey = EncryptionRSA.parseRSAPublicKeyFromBase64(encryptionKeyRSA.getPublicKeyBase64());
        System.out.println(publicKey.toString());
    }

    /**
     * Method: parseRSAPrivateKey(BigInteger modulus, BigInteger exponent)
     */
    @Test
    public void testParseRSAPrivateKey() throws Exception {
        EncryptionRSA.EncryptionKeyRSA encryptionKeyRSA = EncryptionRSA.generatorRSAKey(1024, "mykey".getBytes());
        RSAPrivateKey privateKey = EncryptionRSA.parseRSAPrivateKey(encryptionKeyRSA.getModulus(), encryptionKeyRSA.getPrivateExponent());
        System.out.println(privateKey.toString());
    }

    /**
     * Method: parseRSAPrivateKeyFromHex(String primaryKeyHex)
     */
    @Test
    public void testParseRSAPrivateKeyFromHex() throws Exception {
        EncryptionRSA.EncryptionKeyRSA encryptionKeyRSA = EncryptionRSA.generatorRSAKey(1024, "mykey".getBytes());
        RSAPrivateKey privateKey = EncryptionRSA.parseRSAPrivateKeyFromHex(encryptionKeyRSA.getPrivateKeyHex());
        System.out.println(privateKey.toString());
    }

    /**
     * Method: parseRSAPrivateKeyFromBase64(String primaryKeyHex)
     */
    @Test
    public void testParseRSAPrivateKeyFromBase64() throws Exception {
        EncryptionRSA.EncryptionKeyRSA encryptionKeyRSA = EncryptionRSA.generatorRSAKey(1024, "mykey".getBytes());
        RSAPrivateKey privateKey = EncryptionRSA.parseRSAPrivateKeyFromBase64(encryptionKeyRSA.getPrivateKeyBase64());
        System.out.println(privateKey.toString());
    }

    @Test
    public void test() throws Exception {
        String sourceStr = "hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!hello 世界!";
        byte[] sourceBytes = sourceStr.getBytes(Encryption.ENCODING_UTF8);
        String mykeyStr = "mykey";
        byte[] mykeyBytes = mykeyStr.getBytes(Encryption.ENCODING_UTF8);
        byte[] encodeBytes;
        String encodeStr;
        byte[] decodeBytes;
        String decodeStr;
        byte[] signBytes;
        String signHex;
        String signBase64;
        boolean verify;

        EncryptionRSA.EncryptionKeyRSA encryptionKeyRSA = EncryptionRSA.generatorRSAKey(1024, mykeyBytes);
        System.out.println("RSA-base64公钥：\t" + encryptionKeyRSA.getPublicKeyBase64());
        System.out.println("RSA-base64私钥：\t" + encryptionKeyRSA.getPrivateKeyBase64());

        RSAPublicKey publicKey = EncryptionRSA.parseRSAPublicKey(encryptionKeyRSA.getModulus(), encryptionKeyRSA.getPublicExponent());
        RSAPrivateKey privateKey = EncryptionRSA.parseRSAPrivateKey(encryptionKeyRSA.getModulus(), encryptionKeyRSA.getPrivateExponent());

        /*
            hex
         */
        //公钥加密-hex
        encodeBytes = EncryptionRSA.encryptRSA(publicKey, sourceBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("RSA-hex公钥加密后1：\t" + encodeStr);

        encodeStr = EncryptionRSA.encryptRSAToHex(publicKey, sourceBytes);
        System.out.println("RSA-hex公钥加密后2：\t" + encodeStr);

        encodeStr = EncryptionRSA.encryptRSAToHex(publicKey, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex公钥加密后3：\t" + encodeStr);

        encodeStr = EncryptionRSA.encryptRSAToHex(publicKey, sourceStr);
        System.out.println("RSA-hex公钥加密后4：\t" + encodeStr);

        //私钥解密
        decodeBytes = EncryptionRSA.decryptRSA(privateKey, encodeBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("RSA私钥解密后：\t" + decodeStr);

        //私钥解密-hex
        decodeBytes = EncryptionRSA.decryptRSAFromHex(privateKey, encodeStr);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex私钥解密后1：\t" + decodeStr);

        decodeStr = EncryptionRSA.decryptRSAFromHexToString(privateKey, encodeStr);
        System.out.println("RSA-hex私钥解密后2：\t" + decodeStr);

        decodeStr = EncryptionRSA.decryptRSAFromHexToString(privateKey, encodeStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex私钥解密后3：\t" + decodeStr);

        decodeStr = EncryptionRSA.decryptRSAFromHexToString(privateKey, encodeStr);
        System.out.println("RSA-hex私钥解密后4：\t" + decodeStr);

        //私钥加密-hex
        encodeBytes = EncryptionRSA.encryptRSA(privateKey, sourceBytes);
        encodeStr = Encryption.bytesToHex(encodeBytes);
        System.out.println("RSA-hex私钥加密后1：\t" + encodeStr);

        encodeStr = EncryptionRSA.encryptRSAToHex(privateKey, sourceBytes);
        System.out.println("RSA-hex私钥加密后2：\t" + encodeStr);

        encodeStr = EncryptionRSA.encryptRSAToHex(privateKey, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex私钥加密后3：\t" + encodeStr);

        encodeStr = EncryptionRSA.encryptRSAToHex(privateKey, sourceStr);
        System.out.println("RSA-hex私钥加密后4：\t" + encodeStr);

        //公钥解密-hex
        decodeBytes = EncryptionRSA.decryptRSAFromHex(publicKey, encodeStr);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex公钥解密后1：\t" + decodeStr);

        decodeStr = EncryptionRSA.decryptRSAFromHexToString(publicKey, encodeStr);
        System.out.println("RSA-hex公钥解密后2：\t" + decodeStr);

        decodeStr = EncryptionRSA.decryptRSAFromHexToString(publicKey, encodeStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex公钥解密后3：\t" + decodeStr);

        decodeStr = EncryptionRSA.decryptRSAFromHexToString(publicKey, encodeStr);
        System.out.println("RSA-hex公钥解密后4：\t" + decodeStr);

        /*
            base64
         */
        //公钥加密-base64
        encodeBytes = EncryptionRSA.encryptRSA(publicKey, sourceBytes);
        encodeStr = Encryption.encryptBASE64ToString(encodeBytes);
        System.out.println("RSA-base64公钥加密后1：\t" + encodeStr);

        encodeStr = EncryptionRSA.encryptRSAToBase64(publicKey, sourceBytes);
        System.out.println("RSA-base64公钥加密后2：\t" + encodeStr);

        encodeStr = EncryptionRSA.encryptRSAToBase64(publicKey, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64公钥加密后3：\t" + encodeStr);

        encodeStr = EncryptionRSA.encryptRSAToBase64(publicKey, sourceStr);
        System.out.println("RSA-base64公钥加密后4：\t" + encodeStr);

        //私钥解密
        decodeBytes = EncryptionRSA.decryptRSA(privateKey, encodeBytes);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("RSA私钥解密后：\t" + decodeStr);

        //私钥解密-base64
        decodeBytes = EncryptionRSA.decryptRSAFromBase64(privateKey, encodeStr);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64私钥解密后1：\t" + decodeStr);

        decodeStr = EncryptionRSA.decryptRSAFromBase64ToString(privateKey, encodeStr);
        System.out.println("RSA-base64私钥解密后2：\t" + decodeStr);

        decodeStr = EncryptionRSA.decryptRSAFromBase64ToString(privateKey, encodeStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64私钥解密后3：\t" + decodeStr);

        decodeStr = EncryptionRSA.decryptRSAFromBase64ToString(privateKey, encodeStr);
        System.out.println("RSA-base64私钥解密后4：\t" + decodeStr);

        //私钥加密-base64
        encodeBytes = EncryptionRSA.encryptRSA(privateKey, sourceBytes);
        encodeStr = Encryption.encryptBASE64ToString(encodeBytes);
        System.out.println("RSA-base64私钥加密后1：\t" + encodeStr);

        encodeStr = EncryptionRSA.encryptRSAToBase64(privateKey, sourceBytes);
        System.out.println("RSA-base64私钥加密后2：\t" + encodeStr);

        encodeStr = EncryptionRSA.encryptRSAToBase64(privateKey, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64私钥加密后3：\t" + encodeStr);

        encodeStr = EncryptionRSA.encryptRSAToBase64(privateKey, sourceStr);
        System.out.println("RSA-base64私钥加密后4：\t" + encodeStr);

        //公钥解密-base64
        decodeBytes = EncryptionRSA.decryptRSAFromBase64(publicKey, encodeStr);
        decodeStr = new String(decodeBytes, Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64公钥解密后1：\t" + decodeStr);

        decodeStr = EncryptionRSA.decryptRSAFromBase64ToString(publicKey, encodeStr);
        System.out.println("RSA-base64公钥解密后2：\t" + decodeStr);

        decodeStr = EncryptionRSA.decryptRSAFromBase64ToString(publicKey, encodeStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64公钥解密后3：\t" + decodeStr);

        decodeStr = EncryptionRSA.decryptRSAFromBase64ToString(publicKey, encodeStr);
        System.out.println("RSA-base64公钥解密后4：\t" + decodeStr);

        /*
            签名、签名验证
         */
        signBytes = EncryptionRSA.signRSA(EncryptionRSA.SIGN_ALGORITHMS_MD5, privateKey, sourceBytes);
        System.out.println("RSA-hex签名1：\t" + Encryption.bytesToHex(signBytes));
        signHex = EncryptionRSA.signRSAToHex(EncryptionRSA.SIGN_ALGORITHMS_MD5, privateKey, sourceBytes);
        System.out.println("RSA-hex签名2：\t" + signHex);
        signHex = EncryptionRSA.signRSAToHex(EncryptionRSA.SIGN_ALGORITHMS_MD5, privateKey, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-hex签名3：\t" + signHex);
        signHex = EncryptionRSA.signRSAToHex(EncryptionRSA.SIGN_ALGORITHMS_MD5, privateKey, sourceStr);
        System.out.println("RSA-hex签名4：\t" + signHex);

        signBytes = EncryptionRSA.signRSA(EncryptionRSA.SIGN_ALGORITHMS_MD5, privateKey, sourceBytes);
        System.out.println("RSA-base64签名1：\t" + Encryption.encryptBASE64(signBytes));
        signBase64 = EncryptionRSA.signRSAToBase64(EncryptionRSA.SIGN_ALGORITHMS_MD5, privateKey, sourceBytes);
        System.out.println("RSA-base64签名2：\t" + signBase64);
        signBase64 = EncryptionRSA.signRSAToBase64(EncryptionRSA.SIGN_ALGORITHMS_MD5, privateKey, sourceStr, Encryption.ENCODING_UTF8);
        System.out.println("RSA-base64签名3：\t" + signBase64);
        signBase64 = EncryptionRSA.signRSAToBase64(EncryptionRSA.SIGN_ALGORITHMS_MD5, privateKey, sourceStr);
        System.out.println("RSA-base64签名4：\t" + signBase64);

        verify = EncryptionRSA.verifyRSA(EncryptionRSA.SIGN_ALGORITHMS_MD5, publicKey, sourceBytes, signBytes);
        System.out.println("RSA验签：\t" + verify);
        verify = EncryptionRSA.verifyRSAFromHex(EncryptionRSA.SIGN_ALGORITHMS_MD5, publicKey, sourceBytes, signHex);
        System.out.println("RSA验签-hex：\t" + verify);
        verify = EncryptionRSA.verifyRSAFromBase64(EncryptionRSA.SIGN_ALGORITHMS_MD5, publicKey, sourceBytes, signBase64);
        System.out.println("RSA验签-base64：\t" + verify);
    }

    @Test
    public void test2() throws Exception {
        String sourceStr = "data";
        byte[] sourceBytes = sourceStr.getBytes(Encryption.ENCODING_UTF8);
        byte[] signBytes;
        boolean verify;
        PublicKey publicKey = EncryptionRSA.parseRSAPublicKeyFromBase64("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC6JvQceir16JeV1gDoDKEsAUNJKWp9HkxImEdpRYaFTrYiHfG01LNpt5nPUjH4uiabGul5Nw3OmxsQm3rpIU8nEevN3rtHFtOSPtv0n/hLtt8WQZf3DVpnU0TxbErIYEq3UYRXxxfreLqspLPv7Dobu6NAI5xnuSAHbptuC2UKEQIDAQAB");
        PrivateKey privateKey = EncryptionRSA.parseRSAPrivateKeyFromBase64("MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALom9Bx6KvXol5XWAOgMoSwBQ0kpan0eTEiYR2lFhoVOtiId8bTUs2m3mc9SMfi6Jpsa6Xk3Dc6bGxCbeukhTycR683eu0cW05I+2/Sf+Eu23xZBl/cNWmdTRPFsSshgSrdRhFfHF+t4uqyks+/sOhu7o0AjnGe5IAdum24LZQoRAgMBAAECgYBNCmAPBRSQj0FlFptrbgaqAp/JQKW8wHRLuxIa5FZHB4tjVUc6UqkqAH1ciyT5Tnk5ygqESx+guRqbZe2ZXFq4GM7rjHkpYl8UnbBaDudp/499vLB2VCFxkk16+6MpHKnSOxNRP+j53satybAAIclyvLlMExN7QRLzxodZKAF3XQJBAOU70RaeiTLuRlR7zXultRfZhQ4u3TYxlMJ8PfgPz6aC3d42Oh/5CV/4wXgn5HFr/zGOwZM9uhhZDIhSIsrsf9MCQQDP41uT/uYLwmvlMbVgnuFr4SG/30ev4O6x09+bLBR8Zx/09Nf095Vaz/BcmbHQ7QIU6Q91xFZHcKg5LITcasQLAkA3xaLsduv4iUJxQaHP6JQz1kdqGPrXOZ7w5puJJAeogoSKkPT5XHTsdbBUlJgfBGCVZR8xvL3vOJM1A47Vgk7jAkEAlrLl6/7XnKavuFG0ffouxxlIceLWALU500cXzVDC+Pt4uwXSlw3zAwXB5B62PBHTdH0Oa/yL3vXXvLp9BZuPyQJBAOILAApgyN7Q599rjhBKvdBpDm9hUEWNnN+Hs/ZpKRLfAQm4EPV+uiClS5NW8U+c35+8yzWO/u/zuu23Y7xWd1M=");
        signBytes = EncryptionRSA.signRSA(EncryptionRSA.SIGN_ALGORITHMS_MD5, privateKey, sourceBytes);
        verify = EncryptionRSA.verifyRSA(EncryptionRSA.SIGN_ALGORITHMS_MD5, publicKey, sourceBytes, signBytes);
        System.out.println(String.valueOf(verify));
    }
}
