# btg-util

#### 项目介绍
> 工具

#### 升级记录
```
4.0.24
1、JsonQ扩展；

4.0.23
1、StringBuilder、StringBuffer判空修复；

4.0.22
1、encrypt组件优化；

4.0.21
1、SignUtil优化；

4.0.20
1、FileUtil增加文件及文件夹判断；

4.0.19
1、新增工具：JsonQ、JsonQArray；

4.0.18
1、新增签名工具类：SignUtil；

4.0.17
1、新增url解析工具类：UrlUtil；

4.0.16
1、修复EmptyUtil对于Collection子类非List报错的bug；

4.0.15
1、StringUtil不限定分隔符长度；

4.0.14
1、重构EmptyUtil，俭省代码支持byte数组；

4.0.13
1、重构PathUtil；
2、重构encrypt模块，进化为内部类+静态访问；
3、静态sutil中的类添加私有方法，禁止被new；

v4.0.12
1、void clearDir(File file)默认清除自身；
2、void clearDir(String filePath)默认清除自身；
3、void copyToDir(File file, File targetDir, boolean containsRoot)系列方法重构，为文件夹时支持是否包含自身文件夹；
4、FileUtil的String readToString(InputStream is)默认为UTF-8编码；
5、String readToString(File file)默认为UTF-8编码；
6、String readToString(String filePath)默认为UTF-8编码；
7、void write(String filePath, String content)默认为UTF-8编码；

v4.0.11
1、增加SerializableUtil，处理java序列化；
2、降低依赖，去除log框架

v4.0.10
1、增加PathUtil，FileScanner使用PathUtil获得默认扫描路径；

v4.0.9
1、DateUtil增加获取指定日期所在周起始日期、结束日期；

v4.0.8
1、FileUtil文件拷贝支持path、path文件混用；

v4.0.7
1、FileUtil支持文件或文件夹拷贝到目标文件夹；

v4.0.6
1、FileUtil支持删除文件或文件夹；
2、FileUtil支持清空文件夹；

v4.0.5
1、FileUtil中的智能创建文件夹、文件的方法支持字符串类型path；

v4.0.4
1、FileUtil增加智能创建文件夹、文件的方法；

v4.0.3
1、File的getClassName逻辑修复；
2、引入日志框架slf4j，替换系统打印；

v4.0.2
1、FileUtil写入文件支持文件夹不存在时自动创建；

v4.0.1
1、转为独立maven依赖，去掉parent；

v3.0.4
1、btg-parent升级到v2.0.1

v3.0.3
1、FileUtil增加流的支持
2、FileScanner微调

v3.0.2
1、统一依赖管理

v3.0.1
1、统一迁移至公司名下

v2.0.3
1、整理包名

v2.0.2
1、去除log框架，改为异常处理

v2.0.1
1、项目转为maven项目
2、jdk要求1.8+
3、base64使用java8自带的算法
4、加强了代码规范

v1.2.1
1、Encrption基础加密组件整理
2、增加RSA加密解密、签名验签
3、FileUtil增加文件读写

v1.1.1
1、增加unicode编码解码工具类

v1.0.9
1、拷贝文件自动创建目标文件目录

v1.0.8
1、增加FileUtil，提供文件拷贝、获取文件后缀名
2、增加IOUtil，提供InputStream到byte[]的转换
3、增加StringUtil，提供字符串格式化固定长度
4、去除dist

v1.0.7
1、EmptyUtil验证非空增加数组

v1.0.6
1、Identities的uuid变更为大写

v1.0.5.201706151554
1、考虑到jar包以后维护可能会比较频繁，特统一版本号命名方式

v1.0.4：
1、为了减少依赖，EmptyUtil去掉对fastjson包中的JSONObject、JSONArray的验证

v1.0.3：
1、修复了FileScanner若干bug

v1.0.2：
1、Encryption统一了base64的方法名称
2、增加了文件扫描工具类，实现路径下文件扫描、jar包内文件扫描

v1.0.1：
1、日期处理
2、空值判断
3、加密解密
4、主键、随机字符串生成
```
---
豆圆